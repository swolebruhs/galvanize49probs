def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):

    if x >= rect_x:
        if y >= rect_y:
            if x <= rect_x + rect_width:
                if y <= rect_y + rect_height:
                    return True

    return False
