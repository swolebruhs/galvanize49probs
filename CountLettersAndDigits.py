def count_letters_and_digits(s):

    if len(s) == 0:
        return 0, 0

    count_digit = 0
    count_alpha = 0

    for i in range(len(s)):
        if s[i].isdigit():
            count_digit += 1
        elif s[i].isalpha():
            count_alpha += 1

    return count_alpha, count_digit
        