def find_second_largest(values):
    if len(values) == 0 or len(values) == 1:
        return None

    max_value = max(values)
    second_max_list = []

    for num in values:
        if num != max_value:
            second_max_list.append(num)
        else:
            continue

    return max(second_max_list)

