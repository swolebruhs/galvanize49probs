def max_in_list(values):

    if len(values) == 0:
        return None

    return max(values)
