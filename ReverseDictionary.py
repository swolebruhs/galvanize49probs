def reverse_dictionary(dictionary):


    for key, value in dictionary.items():
        temp = key
        key = value
        value = temp

    return dictionary


dictionary = {
    'a': 1,
    'b': 2,
    'c': 3
}
print(reverse_dictionary(dictionary))
