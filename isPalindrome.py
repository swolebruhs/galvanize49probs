def is_palindrome(word):

    word = word.lower() #Make it case-insensitive

    return word == ''.join(reversed(word))
    