def gear_for_day(is_workday, is_sunny):

    newList = []

    if is_sunny == False and is_workday == True:
        newList.append("umbrella")

    if is_workday == True:
        newList.append("laptop")

    if is_workday == False:
        newList.append("surfboard")

    return newList

    