def pad_left(number, length, pad):

    #Convert the number to a string
    convert_number = str(number)

    #Calculate the number of padding characters needed
    padding_needed = length - len(convert_number)

    #If no padding is needed or if the desired length is less than the number string's length
    if padding_needed <= 0:
        return convert_number

    padding_string = pad * padding_needed
    return padding_string + convert_number
