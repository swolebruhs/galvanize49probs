def calculate_average(values):

    if len(values) == 0:
        return None

    count = 0
    for num in values:
        count += num

    return count / len(values)

