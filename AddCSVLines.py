def add_csv_lines(lines):
    sums = []  # Initialize an empty list to store the sums

    for line in lines:
        numbers = line.split(',')  # Split the line by commas to get individual numbers
        total = 0  # Initialize a variable to store the sum

        for num in numbers:
            total += int(num)  # Convert each number from string to integer and add it to the total

        sums.append(total)  # Append the sum to the list of sums
    return sums


# Test the function
# print(add_csv_lines([]))  # Output: []
print(add_csv_lines(["3", "1,9"]))  # Output: [3, 10]
# print(add_csv_lines(["8,1,7", "10,10,10", "1,2,3"]))  # Output: [16, 30, 6]
