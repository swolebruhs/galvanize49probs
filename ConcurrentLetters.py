from typing import List

def concurrLetters(s: str):

    if len(s) == 0:
        return None

    result = ""

    map = {}
    for letter in s:
        if letter != map[letter]:
            result += map['letter'] + map[letter]
            map.clear()
        map[letter] = map.get(letter, 0) + 1
    return result


print(concurrLetters("aaabaacccbbbbb"))
