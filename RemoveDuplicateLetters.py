def remove_duplicate_letters(s):

    if len(s) == 0:
        return s


    listSet = set()
    result = ""

    for char in s:
        if char in set:
            continue
        else:
            listSet.add(char)
            result += char

    return result
