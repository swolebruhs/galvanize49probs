def has_quorum(attendees_list, members_list):

    return len(attendees_list) >= len(members_list) / 2
