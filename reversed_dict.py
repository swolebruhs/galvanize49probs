def reverse_dictionary(dictionary):

    reversed_dict = {}

    #Loop over the original dictionary
    for key, value in dictionary.items():
        #Assign the key to the value in the new dictionary along with the value
        reversed_dict[value] = key

    return reversed_dict
        