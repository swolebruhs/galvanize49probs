def is_divisible_by_3(number):

    if number % 3 == 0:
        return "fizz"

    return number
