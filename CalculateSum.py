def calculate_sum(values):

    if len(values) == 0:
       return None


    count = 0
    for num in values:
        count += num

    return count
