def sum_of_squares(values):

    if len(values) == 0:
        return None

    sum_total = 0

    for num in values:
        sum_total += num ** 2

    return sum_total
        