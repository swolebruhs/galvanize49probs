def max_of_three(value1, value2, value3):

    max_num = max(value1, value2, value3)

    if value1 == value2 and value2 == value3:
        return value1

    elif value1 == value2:
        return value1

    elif value1 == value3:
        return value1

    elif value2 == value3:
        return value2

    else:
        return max_num


print(max_of_three(4, 21, 4))
