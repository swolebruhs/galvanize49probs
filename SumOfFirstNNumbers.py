def sum_of_first_n_numbers(limit):

    if limit < 0:
        return None

    count_sum = 0
    index = 0
    while index <= limit:
        count_sum += index
        index += 1

    return count_sum
        